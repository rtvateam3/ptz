# ptz

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Installation](#installation)
- [running the app](#running-the-app )

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## Installation

Clone the repo
```sh
git clone "https://gitlab.com/rtvateam3/ptz.git"
```
##### Download the newest version of Cmake

###### On mac / linux
https://github.com/Kitware/CMake/releases/download/v3.14.4/cmake-3.14.4-Darwin-x86_64.dmg 

###### On windows 
https://github.com/Kitware/CMake/releases/download/v3.14.4/cmake-3.14.4-win64-x64.zip

"Select system path for all users" on the installion process 

##### Download newest verison of python
###### On mac / linux
https://www.python.org/ftp/python/3.7.3/python-3.7.3-macosx10.9.pkg

###### On windows 
https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe



##### Download the libraries listed below with pip
- Flask
- opencv
- face-recognition
- dlib
- numpy
```sh
pip install Flask opencv-python numpy face-recognition dlib
```

## Running-the-app 

Start webserver
```sh
FLASK_APP=hello.py flask run
```

Visit the web interface on http://127.0.0.1:5000/  