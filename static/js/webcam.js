'use strict';
// get stream block elements
var videoElement = document.querySelector('.webcamSmall');
var videoElementTwo = document.querySelector('.webcamBig');

navigator.mediaDevices.enumerateDevices()
  .then(getStream).catch(handleError);

// get content from webcam
function getStream() {
  if (window.stream) {
    window.stream.getTracks().forEach(function(track) {
      track.stop();
    });
  }

  var constraints = {
    video: {
    	// webcam id changes this with other camera
      deviceId: {exact: "d57d2beea0f7ead250c9d9ad3532aed9e9d0bd644b09603003f3638c4e54c266"}
    }
  };

  navigator.mediaDevices.getUserMedia(constraints).
    then(gotStream).catch(handleError);
}

//Display the stream 
function gotStream(stream) {
  window.stream = stream; // make stream available to console
   videoElement.srcObject = stream;
  videoElementTwo.srcObject = stream;
}

function handleError(error) {
  console.log('Error: ', error);
}