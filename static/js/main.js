// get Elemetns from html page and save this to variables
var cam_1_title = document.querySelector("#cam_1 .stream-block-title");
var cam_2_title = document.querySelector("#cam_2 .stream-block-title");
var cam_3_title = document.querySelector("#cam_3 .stream-block-title");
var cam_4_title = document.querySelector("#cam_4 .stream-block-title");
var activecam_title = document.querySelector("#activecam .stream-block-title");
var activecam_name = document.querySelector("#activecam #name");
var activecam_name = document.querySelector("#activecam #name");
var activecam_video = document.querySelector("#activecam .stream-block .video-placeholder");
var activecam_number = 1;
var settings_cam_1 = {naam:"Camera 1", facetracking:false};
var settings_cam_2 = {naam:"Camera 2", facetracking:false};
var settings_cam_3 = {naam:"Camera 3", facetracking:false};
var settings_cam_4 = {naam:"Camera 4", facetracking:true};
var settings_activecam = settings_cam_1;
var activecam_facetracking = document.querySelector("#activecam #facetracking");


//checks when page is loaded
$( ".details-midle " ).ready(function() {
    changeCam(1);
	startup();
});

// init function for sliders
function startup(){
	$("#ex18X").slider({
		min: 0,
		max: 100,
		value: [30, 70],
	});
	$("#ex18Y").slider({
		min: 0,
		max: 100,
		value: [35, 68],
	});
}

// send movement command to camera 
function go(direction) {
	if (activecam_number === 1 && settings_activecam.facetracking === false) {
		console.log("gaat " + direction)
		$.ajax({
		    url: "http://192.168.137.115:443/cgi-bin/hi3510/ptzctrl.cgi?-step=0&-act=" + direction ,
		    type: "GET",
		    // This is the important part
		    xhrFields: {
		        withCredentials: true
		    },
		    // This is the important part
		    data: "data",
		    success: function (response) {
		        // handle the response
		    },
		    error: function (xhr, status) {
		        // handle errors
		    }
		});
	}
}

// send movement command to camera 
function send(event) {
	if (activecam_number == 1 && settings_activecam.facetracking == false) {
		if(event == "stop" ){
			go("stop");
		}	else if (event.keyCode == 68 || event.keyCode == 39){
			go("right");
		}	else if (event.keyCode == 83 || event.keyCode == 40){
			go("down");
		}	else if (event.keyCode == 65 || event.keyCode == 37){
			go("left");
		}	else if (event.keyCode == 87 || event.keyCode == 38){
			go("up");
		}
	}
}

// send preset movement to camera 
function preset(value){
	var direction_id = "";
	if(value == "center" ){
		direction_id = 0;
	}	else if (value == "left"){
		direction_id = 1;
	}	else if (value == "right"){
		direction_id = 2;
	}
	$.ajax({
	    url: "http://192.168.137.115:443//cgi-bin/hi3510/preset.cgi?-act=goto&-number=" + direction_id,
	    type: "GET",
	    // This is the important part
	    xhrFields: {
	        withCredentials: true
	    },
	    // This is the important part
	    data: "data",
	    success: function (response) {
	        // handle the response
	    },
	    error: function (xhr, status) {
	        // handle errors
	    }
	});
}

// update the value in facetrackingStatus.txt.txt with the value of facetracking
function sendFacetracking(value){
	if (value == true) {
		document.querySelector(".details-overlay").classList.add("show-overlay");
	}
	else {
		document.querySelector(".details-overlay").classList.remove("show-overlay");
	}
	$.ajax ({
		data: {
			facetracking:value
		},
		type: 'POST',
		url: '/server'
	})

}

// swap between camp triggerd with onclick event 
function changeCam(number) {
	var activecam_toggle = document.querySelector("#activecam .toggle");
	// last stream hiding and add display show to active cam
	if (activecam_number == 1){
		document.querySelector(".active_cam_1").classList.remove("display-show");
		document.querySelector(".active_cam_1").classList.add("display-none");
	} else if (activecam_number == 2){
		document.querySelector(".active_cam_2").classList.remove("display-show");
		document.querySelector(".active_cam_2").classList.add("display-none");
	} else if (activecam_number == 3){
		document.querySelector(".active_cam_3").classList.remove("display-show");
		document.querySelector(".active_cam_3").classList.add("display-none");
	}else if (activecam_number == 4){
		document.querySelector(".active_cam_4").classList.remove("display-show");
		document.querySelector(".active_cam_4").classList.add("display-none");
	}

	if (number == 1){
		document.querySelector(".active_cam_1").classList.remove("display-none");	
		document.querySelector(".active_cam_1").classList.add("display-show");
		settings_activecam = settings_cam_1;
	} else if (number == 2){
		document.querySelector(".active_cam_2").classList.remove("display-none");	
		document.querySelector(".active_cam_2").classList.add("display-show");
		settings_activecam = settings_cam_2;
	}else if (number == 3){
		document.querySelector(".active_cam_3").classList.remove("display-none");	
		document.querySelector(".active_cam_3").classList.add("display-show");
		settings_activecam = settings_cam_3;
	}else if (number == 4){
		document.querySelector(".active_cam_4").classList.remove("display-none");	
		document.querySelector(".active_cam_4").classList.add("display-show");		
		settings_activecam = settings_cam_4;
	}

	activecam_number = number;
	activecam_title.innerHTML = settings_activecam.naam;
	activecam_name.value =  settings_activecam.naam;
	activecam_facetracking.checked = settings_activecam.facetracking;
	if (activecam_facetracking.checked == true) {
		activecam_toggle.classList.add("btn-primary");
		activecam_toggle.classList.remove("btn-default");
		activecam_toggle.classList.remove("off");
	} else if(activecam_facetracking.checked == false){
		activecam_toggle.classList.remove("btn-primary");
		activecam_toggle.classList.add("btn-default");
		activecam_toggle.classList.add("off");
	}
}


// call the function add from at the server
function addFace(argument) {	
	if (argument == "again") {
		$('#myModal').modal("hide");
	}

	random = Math.floor(Math.random() * 1000);
	$.ajax ({
		data: {
			randomnumber:random
		},
		type: 'POST',
		url: '/add-face',
	})
	
	setTimeout(function(){
		openModal()
	}, 3000);

	function openModal(){
		$('#faceImg').attr("src","/static/volg_"+random+".png");
	  	$('#myModal').modal("show");
	}	
}

// update the value in marginsettings.txt with the value of margin slider
function saveMargin(){
	var min_x = $(".marginX .tooltip-min .tooltip-inner")[0].innerHTML;
	var max_x = $(".marginX .tooltip-max .tooltip-inner")[0].innerHTML;
	var min_y = $(".marginY .tooltip-min .tooltip-inner")[0].innerHTML;
	var max_y = $(".marginY .tooltip-max .tooltip-inner")[0].innerHTML;
	 $("#xMin")[0].x1.baseVal.valueInSpecifiedUnits = min_x;
	 $("#xMin")[0].x2.baseVal.valueInSpecifiedUnits = min_x;

	 $("#xMax")[0].x1.baseVal.valueInSpecifiedUnits = max_x;
	 $("#xMax")[0].x2.baseVal.valueInSpecifiedUnits = max_x;

	 $("#yMin")[0].y1.baseVal.valueInSpecifiedUnits = 100 - min_y;
	 $("#yMin")[0].y2.baseVal.valueInSpecifiedUnits = 100 - min_y;

	 $("#yMax")[0].y1.baseVal.valueInSpecifiedUnits = 100 - max_y;
	 $("#yMax")[0].y2.baseVal.valueInSpecifiedUnits = 100 - max_y;

	 min_x = Math.round(min_x * 6.4);
	 max_x = Math.round(max_x * 6.4);
	 min_y = Math.round(min_y * 4.8);
	 max_y = Math.round(max_y * 4.8);

	$.ajax ({
		data: {
			xMin: min_x,
			xMax: max_x,
			yMin: min_y,
			yMax: max_y
		},
		type: 'POST',
		url: '/set-margin',
	})

}

// save the setting from face tracking switch
function save() {
	if (activecam_number == 1){
		settings_cam_1.facetracking = activecam_facetracking.checked;
		sendFacetracking(activecam_facetracking.checked);
		settings_cam_1.naam = activecam_name.value;
		cam_1_title.innerHTML = activecam_name.value;
	} else if (activecam_number == 2){
		settings_cam_2.facetracking = activecam_facetracking.checked;
		settings_cam_2.naam = activecam_name.value;
		cam_2_title.innerHTML = activecam_name.value;
	}else if (activecam_number == 3){
		settings_cam_3.facetracking = activecam_facetracking.checked;
		settings_cam_3.naam = activecam_name.value;
		cam_3_title.innerHTML = activecam_name.value;
	}else if (activecam_number == 4){
		settings_cam_4.facetracking = activecam_facetracking.checked;
		settings_cam_4.naam = activecam_name.value;
		cam_4_title.innerHTML = activecam_name.value;
	}
	activecam_title.innerHTML = activecam_name.value;
}


// Replace source when image not found
$('img#faceImg').on("error", function() {
  $(this).attr('src', "./static/img/factrackingStatus.jpg");
});
