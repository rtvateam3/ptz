import cv2
import os

def add_face(random):
    # de map waar oude fotos moeten verwijd worden 
    files = os.listdir("./static")
    for file in files:
        if "volg_" in file:
            os.remove("./static/" + file)
            
    video=cv2.VideoCapture(0)

    #verwijzen naar de trainer file
    face_cascade=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    
    check, frame=video.read()
    #bepaal de parameters van het gezicht
    faces = face_cascade.detectMultiScale(frame, scaleFactor=1.03, minNeighbors=5)
    
    for x,y,w,h in faces:
        
        #de coordinaten het gezicht toepassen
        crop_img=frame[y:y+h,x:x+w]
        
        #de geknipte foto opslaan
        cv2.imwrite("./FOLLOWTHISPERSON/volg.png",crop_img)
        cv2.imwrite("./static/volg_" + random + ".png", crop_img)

    video.release()
    print("Capture succeeded")
