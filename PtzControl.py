#Imports
import Facerecognition
import cv2
import requests
import time

#Capture source
video_capture = cv2.VideoCapture(0)
video_capture.open(0)

#Urls
urlPresetMove = "http://192.168.137.115:443/cgi-bin/hi3510/preset.cgi?-act=goto&-number=0"
urlAbsoluteMove = "http://192.168.137.115:443/cgi-bin/hi3510/ptzctrl.cgi?-step=1&-act="
urlContinuousMove = "http://192.168.137.115:443/cgi-bin/hi3510/ptzctrl.cgi?-step=0&-act="
headers = {'Authorization':'Basic YWRtaW46aXBjYW0='}

#Control variabelen
left = "left"
right = "right"
up = "up"
down = "down"

#Open files
status = open("facetrackingStatus.txt", "r")
margin = open("marginsettings.txt", "r")

#Methods ptz control
def movePreset():
    requests.get(urlPresetMove, headers=headers)

def moveLeftAbsolute():
    requests.get(urlAbsoluteMove + str(left), headers=headers)

def moveRightAbsolute():
    requests.get(urlAbsoluteMove + str(right), headers=headers)

def moveUpAbsolute():
    requests.get(urlAbsoluteMove + str(up), headers=headers)

def moveDownAbsolute():
    requests.get(urlAbsoluteMove + str(down), headers=headers)

#Methods to read status and margin
def readFile():
    status.seek(0,0)
    return status.read()

def readMargins():
    margin.seek(0,0)
    values = margin.read()
    #Default tracking: xMin:192, xMax:448, yMin:168, yMax:326
    xMin, xMax, yMin, yMax = values.split()
    return xMin, xMax, yMin, yMax

#Main code
def main(name):
    while True:
        if (readFile() == "false"):
            time.sleep(2)
        else:
            xMin, xMax, yMin, yMax = readMargins()
            ret, frame = video_capture.read()
            if not ret:
                print("empty")
                continue
            top, rechts, bottom, links = Facerecognition.analyzeFrame(frame) 
            x = ((rechts - links) // 2) + links
            y = ((bottom - top) // 2) + top
            if(x == 0 or y == 0):
               continue
            elif(x > int(xMax)):
                moveRightAbsolute()
            elif(x < int(xMin)):
                moveLeftAbsolute()
            elif(y < int(yMin)):
                moveUpAbsolute()
            elif(y > int(yMax)):
                moveDownAbsolute()
            print("X value: " + str(x))
            print("Y value: " + str(y))
            print()
