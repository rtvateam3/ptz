import face_recognition
import cv2

def analyzeFrame(frame):
    # Load a sample picture and learn how to recognize it.
    person1_image = face_recognition.load_image_file("./FOLLOWTHISPERSON/volg.png")
    person1_face_encoding = face_recognition.face_encodings(person1_image)[0]

    # Create arrays of known face encodings and their names
    known_face_encodings = [
        person1_face_encoding
    ]
    known_face_names = [
        "Presentator"
    ]
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = frame[:, :, ::-1]
    
    # Find all the faces and face encodings in the current frame of video
    face_locations = face_recognition.face_locations(rgb_small_frame)
    face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

    face_names = []
    for face_encoding in face_encodings:
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        name = "Unknown"

        # If a match was found in known_face_encodings, just use the first one.
        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_names[first_match_index]
            face_names.append(name)
            break
        face_names.append(name)
		
    if(len(face_locations) == 0):
        return [0, 0, 0, 0]
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        if (name != 'Unknown'):
            top = top
            right = right
            bottom = bottom
            left = left
            return [top, right, bottom, left]
        else:
            return [0, 0, 0, 0] 
