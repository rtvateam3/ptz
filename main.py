#!/usr/bin/env python3
from flask import Flask
from flask import request
from flask import render_template
import webbrowser
import PtzControl as ptz
import threading
import add_face as add

f = open("facetrackingStatus.txt", "w")
f.write("false")
f.close()
f = open("marginsettings.txt", "w")
f.write("192 448 168 326")
f.close()


app = Flask(__name__, template_folder='template')

@app.route("/")
def mainpage():
	return render_template('index.html')

@app.route('/server', methods=['POST'])
def switchFacerecognition():
	facetracking = request.form['facetracking']
	f = open("facetrackingStatus.txt", "w")
	f.write(facetracking)
	f.close()
	return '0'

@app.route('/add-face', methods=['POST'])
def add_face():
        random = request.form['randomnumber']
        add.add_face(random)
        return '0'

@app.route('/set-margin', methods=['POST'])
def set_margin():
        xMin = request.form['xMin']
        xMax = request.form['xMax']
        yMin = request.form['yMin']
        yMax = request.form['yMax']

        f = open("marginsettings.txt", "w")
        f.write(xMin + " " + xMax + " " + yMin + " " + yMax)
        f.close()
        return '0'
###################################################################
        
thread_1 = threading.Thread(target=ptz.main, args=(1,))
thread_1.start()
webbrowser.open_new("http://127.0.0.1:5000")
